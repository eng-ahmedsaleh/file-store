#!/usr/bin/env node
const { handleCommands } = require("./lib");
const { File } = require("./lib");

const [, , ...args] = process.argv;

// File.name = 'dict.txt'
handleCommands(args);
