
config(Sync) file exists && own permission ? OK : create file

add 
    file.get(key) ? chunk -> file.remove(key) : 
    replace || append
    dispose()
    memCache.add(key, val)

get
    memCache.get(key) ? OK :
    file.get(key)
    {
        file.readableStream()
        dict = chunk2Dict()
        dict.get(key) ? OK && dispose() : repeat
    }

remove
    file.remove(key)
    {
        file.readableStream()
        chunk2Line()
        key === key ? val = undefined replaceLine(val) || clearLine() return true: repeat
        dispose()
        return false
    }
    memCache.remove(key)

list
    if first time insertion (just created)
        memCache.list()
    else
        file.list()
        {
            file.readableStream()
            pipe chunks to stdout
            dispose()
        }

clear
    file.delete()
    dispose()
    memCache.clear()



store.js
dict.txt