"use strict";

const { FgCyan, FgBlack, FgGreen, FgRed } = require("./colors");

function print(title, msg = "", opts, color) {
  opts = opts || console.log;
  color = color || FgGreen;

  opts(`${FgCyan} \t ${title} \n`);
  opts(color, msg);
}

function handleError(err, opts) {
  print("ERROR", err, opts, FgRed);
}

exports = module.exports = {
  print: print,
  handleError: handleError
};
