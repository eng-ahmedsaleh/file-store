"use strict";

const {
  appendFile,
  appendFileSync,
  createReadStream,
  createWriteStream,
  unlink,
  writeFile
} = require("fs");
const { createInterface } = require("readline");
const { handleError, print } = require("../helper");

const File = (function(opts) {
  opts = opts || {
    encoding: "utf8",
    flags: "r+",
    highWaterMark: 1 * 1024
  };

  let _readLine = { close: () => {} };
  const _path = __dirname + "/../" + "dict.txt";
  const _createIfNotExists = () =>
    writeFile(_path, "", { flag: "wx" }, err => {});

  _createIfNotExists();

  const _add = (key, val) => {
    return new Promise((resolve, reject) => {
      _get(key)
        .then(val => {
          _remove(key);
        })
        .then(() => {
          appendFile(_path, `\n[${key}, ${val}]`, err => {
            if (err) {
              handleError(err);
              return;
            }
            resolve();
          });
        });
      // TODO: Append if not found
    });
  };

  // FIXME: Fix start bug
  const _remove = key => {
    return new Promise((resolve, reject) => {
      let counter = 0;
      let _readableStream = createReadStream(_path, opts);

      _readLine = createInterface({
        input: _readableStream,
        removeHistoryDuplicates: true,
        crlfDelay: Infinity,
        console: false
      });

      _readLine
        .on("line", line => {
          if (key == JSON.parse(line.trim() || "{}")[0]) {
            let _writableStream = createWriteStream(_path, {
              encoding: "utf8",
              flags: "r+",
              start: counter
            });

            let data = "";
            for (let i of line) {
              data += " ";
            }
            data += "\n";
            _writableStream.write(data, () => {
              _dispose(_readableStream, _writableStream, _readLine);
              resolve();
            });
          } else {
            counter += line.length;
          }
        })
        .on("end", () => _dispose(_readableStream, _writableStream, _readLine))
        .on("error", err => handleError(err));
    });
  };

  const _get = key => {
    return new Promise((resolve, reject) => {
      let _readableStream = createReadStream(_path, opts);

      _readLine = createInterface({
        input: _readableStream,
        removeHistoryDuplicates: true,
        crlfDelay: Infinity,
        console: false
      });

      _readLine
        .on("line", line => {
          if (key == JSON.parse(line.trim() || "{}")[0]) {
            const val = JSON.parse(line)[1];
            _disposeGet(_readableStream, _readLine);
            resolve(val);
          }
        })
        .on("end", () => _disposeGet(_readableStream, _readLine))
        .on("error", err => handleError(err));
    });
  };

  const _list = () => {
    let _readableStream = createReadStream(_path, opts);

    _readableStream
      .on("readable", () => {
        let chunk;
        while ((chunk = _readableStream.read())) {
          print("", chunk);
        }
      })
      .on("end", () => _disposeList(_readableStream))
      .on("error", err => handleError(err));
  };

  const _clear = () =>
    new Promise((resolve, reject) =>
      unlink(_path, err => {
        if (err) {
          handleError(err);
          return;
        }
        resolve();
      })
    );

  const _disposeList = _readableStream => {
    _readableStream.destroy();
    _readableStream.removeAllListeners();
  };

  const _disposeGet = (_readableStream, _readLine) => {
    _readableStream.destroy();
    _readableStream.removeAllListeners();
    _readLine.close();
  };

  const _dispose = (_readableStream, _writableStream, _readLine) => {
    _readableStream.destroy();
    _readableStream.removeAllListeners();
    _writableStream.destroy();
    _writableStream.removeAllListeners();
    _readLine.close();
  };

  return {
    Path: _path,
    Add: _add,
    Remove: _remove,
    Get: _get,
    List: _list,
    Clear: _clear
  };
})();

exports = module.exports = File;
