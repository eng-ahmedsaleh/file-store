"use strict";

const MemCache = require("./memcache");
const File = require("./file");
const { print } = require("../helper");

const Dictionary = (function() {
  const add = (key, val) =>
    File.Add(key, val).then(() => MemCache.Add(key, val));

  const remove = key => File.Remove(key).then(() => MemCache.Remove(key));

  const get = key => {
    const data = MemCache.Get(key);
    data
      ? print("DATA FOUND", data)
      : File.Get(key).then(val => print("DATA FOUND", val));
  };

  const list = () => File.List();

  const clear = () => File.Clear().then(() => MemCache.Clear());

  return {
    add: add,
    remove: remove,
    get: get,
    list: list,
    clear: clear
  };
})();

exports = module.exports = Dictionary;
